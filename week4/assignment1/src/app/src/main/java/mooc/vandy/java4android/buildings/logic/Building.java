package mooc.vandy.java4android.buildings.logic;

/**
 * This is the Building class file.
 */
public class Building {

    // TODO - Put your code here.
    private int mLength;

    private int mWidth;

    private int mLotLength;

    private int mLotWidth;

    public Building(int length, int width, int lotLength, int lotWidth) {
        this.mLength = length;
        this.mWidth = width;
        this.mLotLength = lotLength;
        this.mLotWidth = lotWidth;
    }

    public int getLength() {
        return this.mLength;
    }

    public void setLength(final int inMLength) {
        this.mLength = inMLength;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public void setWidth(final int inMWidth) {
        this.mWidth = inMWidth;
    }

    public int getLotLength() {
        return this.mLotLength;
    }

    public void setLotLength(final int inMLotLength) {
        if (inMLotLength > mLength) {
            this.mLotLength = inMLotLength;
        } else {
            this.mLotLength = mLength;
        }
    }

    public int getLotWidth() {
        return this.mLotWidth;
    }

    public void setLotWidth(final int inMLotWidth) {
        if (inMLotWidth > mWidth) {
            mLotWidth = inMLotWidth;
        } else {
            mLotWidth = mWidth;
        }
    }

    public int calcBuildingArea() {
        return mLength * mWidth;
    }

    public int calcLotArea() {
        return mLotLength * mLotWidth;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
