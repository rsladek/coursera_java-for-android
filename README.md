# [Java for Android](https://www.coursera.org/learn/java-for-android/home/welcome)

## Syllabus
***

### Week 1

#### Module 1: MOOC Overview

#### Module 2: Introduction to Android Studio

#### Module 3: Writing a Simple Android App Using Basic Java Features


* Overview of the Java for Android MOOC
* Module 1 Introduction
* MOOC Organization, Contents, and Strategies
* Overview of the Java Programming Language
* Module 1 Summary
* Module 1 Quiz
* Module 2 Introduction
* Installing Java and Android Studio
* Creating and Importing Projects
* Introducing the Editor
* Project File Organization
* Creating and Editing Files
* Creating and Running a Virtual Device
* Debugging, Syntax Highlighting, and Logcat
* Self Assessment & Multi-File Uploading
* Module 2 Summary
* Module 2 Quiz
* Module 3 Introduction
* Variables and types
* Fundamental components
* Creating and calling (static) methods
* Module 3 Quiz
* Programming Assignment


**Quiz:** Module 1 Quiz

**Quiz:** Module 2 Quiz

**Quiz:** Module 3 Quiz

**Programming Assignment:** Geometry Calculations - Auto Graded

***

### Week 2

#### Module 4: Control Flow

#### Module 5: Structured Data

* Introduction to control flow
* Conditional statements: if/else
* Counting loops: for loops
* Indefinite loops: while loops and do-while loops
* Random numbers
* Programming assignment
* Module Summary
* Introduction to common structured data
* Built-in arrays
* For-each loops
* Introduction to the Java Collections Framework
* ArrayLists
* HashMaps
* Structured Data WrapUp
* Programming assignment with structured data

**Quiz:** Module 4 quiz on FOR loops

**Programming Assignment:** Drawing ASCII Art - Auto Graded Portion

**Programming Assignment:** The Birthday Problem - Auto Graded

**Quiz:** Module 5 Quiz

***

### Week 3

#### Module 6: Classes and Interfaces

#### ModuIe 7: Inheritance and Polymorphism

* An overview of the module
* Object Oriented Programming concepts
* Constructing your own class
* Refining your class file by applying principles of OOP
* Overriding methods to improve your class; making code more readable
* Using new objects in old places - arrays and parameter passing
* Java Generics
* Programming Assignment: Gate class
* Programming Assigment: Client Files
* An Overview of the Module
* Introduction to Inheritance
* Continuing with class inheritance
* Exception Handling
* Programming assignment: Inheritance

**Quiz:** Module 6 quiz on Object Oriented Programing concepts

**Quiz:** Module 6 quiz on methods and classes

**Quiz:** Module 6 quiz on arrays and parameters

**Programming Assignment:** Building your own class pt 1 - Auto Graded

**Programming Assignment:** Building your own Classes pt 2 - Auto Graded

**Quiz:** Module 7 quiz on inheritance

**Quiz:** Module 7 quiz on code "dissection"

**Programming Assignment:** Inheritance and Polymorphism Assignment - Auto Graded

***

### Week 4

#### Module 8: Android Calculator App Mini-Project Assignment

* The Calculator App Mini-Project Assignment

**Peer Graded Assignment:** Peer Review: Mini-Project: Calculator App